package com.tt.dto;

public class Customer {
	
	String name;
	int age;
	String gender;
	String shHypertension;
	String shBloodPressure;
	String shBloodsugar;
	String shOverweight;
	String hSmoking;
	String hAlcohol;
	String hDailyExercise;
	String hDrugs;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getShHypertension() {
		return shHypertension;
	}
	public void setShHypertension(String shHypertension) {
		this.shHypertension = shHypertension;
	}
	public String getShBloodPressure() {
		return shBloodPressure;
	}
	public void setShBloodPressure(String shBloodPressure) {
		this.shBloodPressure = shBloodPressure;
	}
	public String getShBloodsugar() {
		return shBloodsugar;
	}
	public void setShBloodsugar(String shBloodsugar) {
		this.shBloodsugar = shBloodsugar;
	}
	public String getShOverweight() {
		return shOverweight;
	}
	public void setShOverweight(String shOverweight) {
		this.shOverweight = shOverweight;
	}
	public String gethSmoking() {
		return hSmoking;
	}
	public void sethSmoking(String hSmoking) {
		this.hSmoking = hSmoking;
	}
	public String gethAlcohol() {
		return hAlcohol;
	}
	public void sethAlcohol(String hAlcohol) {
		this.hAlcohol = hAlcohol;
	}
	public String gethDailyExercise() {
		return hDailyExercise;
	}
	public void sethDailyExercise(String hDailyExercise) {
		this.hDailyExercise = hDailyExercise;
	}
	public String gethDrugs() {
		return hDrugs;
	}
	public void sethDrugs(String hDrugs) {
		this.hDrugs = hDrugs;
	}
	public Customer(String name, int age, String gender, String shHypertension, String shBloodPressure,
			String shBloodsugar, String shOverweight, String hSmoking, String hAlcohol, String hDailyExercise,
			String hDrugs) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.shHypertension = shHypertension;
		this.shBloodPressure = shBloodPressure;
		this.shBloodsugar = shBloodsugar;
		this.shOverweight = shOverweight;
		this.hSmoking = hSmoking;
		this.hAlcohol = hAlcohol;
		this.hDailyExercise = hDailyExercise;
		this.hDrugs = hDrugs;
	}
	public Customer()
	{
		
	}
	@Override
	public String toString() {
		return "Customer [name=" + name + ", age=" + age + ", gender=" + gender + ", shHypertension=" + shHypertension
				+ ", shBloodPressure=" + shBloodPressure + ", shBloodsugar=" + shBloodsugar + ", shOverweight="
				+ shOverweight + ", hSmoking=" + hSmoking + ", hAlcohol=" + hAlcohol + ", hDailyExercise="
				+ hDailyExercise + ", hDrugs=" + hDrugs + "]";
	}
	

}

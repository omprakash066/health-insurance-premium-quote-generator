package com.tt.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tt.dto.Customer;
import com.tt.process.Processor;

/**
 * Servlet implementation class HealthInsurancePremiumCal
 */
@WebServlet("/HealthInsurancePremiumCal")
public class HealthInsurancePremiumCal extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HealthInsurancePremiumCal() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Customer cus=new Customer();
		
		
		cus.setName(request.getParameter("name"));
		cus.setAge(Integer.parseInt(request.getParameter("age")));
		cus.setGender(request.getParameter("gen"));
		cus.sethAlcohol(request.getParameter("alc"));
		cus.sethDailyExercise(request.getParameter("de"));
		cus.sethDrugs(request.getParameter("dr"));
		cus.sethSmoking(request.getParameter("smo"));
		cus.setShBloodPressure(request.getParameter("bpr"));
		cus.setShBloodsugar(request.getParameter("bs"));
		cus.setShHypertension(request.getParameter("hyp"));
		cus.setShOverweight(request.getParameter("ovw"));
		
		Processor process = new Processor();
		int total=process.premiumcalculator(cus);
		
		
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		out.print("<html><body>");
				out.println("Health Insurance Premium for\t"+cus.getName()+":Rs."+total);
				out.print("</body></html>");
	}

}

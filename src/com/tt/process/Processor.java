package com.tt.process;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.tt.dto.Customer;

public class Processor {
	
	
	public int premiumcalculator(Customer cus) throws IOException
	{
		 InputStream inputStream = null;
		 Properties prop = new Properties();
		 float total=0;
			String propFileName = "businessconfig.properties";

		try {

	   
	    inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	    
	    if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
	    total=Float.parseFloat(prop.getProperty("basepremium"));
	    
	    if(cus.getGender().equals("male"))
	    {
	    	String rate=prop.getProperty("genderM");
	    	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	    float habits=0;
	    if(cus.gethAlcohol().equals("yes"))
	    {
	    	String rate=prop.getProperty("Consumptionofalcohol");
	    	habits+=Float.parseFloat(rate);
	    //	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	  
	    if(cus.gethDailyExercise().equals("yes"))
	    {
	    	String rate=prop.getProperty("dailyexcercise");
	    	habits-=Float.parseFloat(rate);
	    //	total=total*(1-(Float.parseFloat(rate)/100));
	    }
	    if(cus.gethDrugs().equals("yes"))
	    {
	    	String rate=prop.getProperty("Drugs");
	    	habits+=Float.parseFloat(rate);
	    //	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	    if(cus.gethSmoking().equals("yes"))
	    {
	    	String rate=prop.getProperty("Smoking");
	    	habits+=Float.parseFloat(rate);
	    //	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	    total=total*(1+(habits/100));
	    if(cus.getShBloodPressure().equals("yes"))
	    {
	    	String rate=prop.getProperty("Blookpressure");
	    	
	    	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	    if(cus.getShBloodsugar().equals("yes"))
	    {
	    	String rate=prop.getProperty("Bloodsugar");
	    	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	    if(cus.getShHypertension().equals("yes"))
	    {
	    	String rate=prop.getProperty("Hypertension");
	    	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	    if(cus.getShOverweight().equals("yes"))
	    {
	    	String rate=prop.getProperty("Overweight");
	    	total=total*(1+(Float.parseFloat(rate)/100));
	    }
	    int age=cus.getAge();
	   if(age>=18)
	   {
		   String rate=prop.getProperty("age18-25");
		   total=total*(1+(Float.parseFloat(rate)/100));
		   if(age>=25)
		   {
			   String rate1=prop.getProperty("age25-30");
			   total=total*(1+(Float.parseFloat(rate1)/100));
		   }
		   if(age>=30)
		   {
			   String rate1=prop.getProperty("age30-35");
			   total=total*(1+(Float.parseFloat(rate1)/100));
		   }
		   if(age>=35)
		   {
			   String rate1=prop.getProperty("age35-40");
			   total=total*(1+(Float.parseFloat(rate1)/100));
		   }
		   if(age>=40)
		   {
			   String rate1=prop.getProperty("age40+");
			   total=total*(1+(Float.parseFloat(rate1)/100));
		   }
	
	   }
		}
		catch (Exception e) {
			System.out.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return Math.round(total);
		
	}

}
